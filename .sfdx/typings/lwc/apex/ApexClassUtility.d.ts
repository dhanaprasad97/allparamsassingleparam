declare module "@salesforce/apex/ApexClassUtility.createDependents" {
  export default function createDependents(param: {dependentString: any}): Promise<any>;
}

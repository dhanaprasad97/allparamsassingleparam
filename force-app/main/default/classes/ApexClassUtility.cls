/***
 * Created by : Dhana Prasad
 * Description : to update or insert record by using a single param to pass all fields
 */
public with sharing class ApexClassUtility {
    /*
     * Method Name: createDependents
     * Purpose: creates a dependent record if no duplicate exist else return duplicate flag as true
     * Parameter: dependentString
     * Return: NA
     */ 
    @AuraEnabled
    public static voi createDependents(String dependentString){

        Dependent__c obj = new Dependent__c();
        try {
            Map<String, String> wrapperMap = (Map<String,String>) JSON.deserialize(dependentString, Map<String,
                String>.class);

           
                // creating a dependent object based on dependentString wrapper here dependent is custom object
                if(wrapperMap.containsKey('coverageId'))
                    obj.put('Coverage__c',wrapperMap.get('coverageId'));
                if(wrapperMap.containsKey('dob') && wrapperMap.get('dob') != '')
                    obj.put('Date_of_Birth__c',Date.valueOf(wrapperMap.get('dob')));
                if(wrapperMap.containsKey('relationShip'))
                    obj.put('Relationship__c',wrapperMap.get('relationShip'));
                if(wrapperMap.containsKey('lastName'))
                    obj.put('Last_Name__c',wrapperMap.get('lastName'));
                 if(wrapperMap.containsKey('suffix'))
                    obj.put('Suffix__c',wrapperMap.get('suffix'));
                if(wrapperMap.containsKey('gender'))
                    obj.put('Gender__c',wrapperMap.get('gender'));
                if(wrapperMap.containsKey('ssn'))
                    obj.put('Social_Security_Number__c',wrapperMap.get('ssn'));
                if(wrapperMap.containsKey('firstName'))
                    obj.put('First_Name__c',wrapperMap.get('firstName'));
                if(wrapperMap.containsKey('middleName'))
                    obj.put('Middle_Name__c',wrapperMap.get('middleName'));
                if(wrapperMap.containsKey('Id'))
                    obj.put('Id',wrapperMap.get('Id'));
                if(wrapperMap.containsKey('spouseRemarried'))
                    obj.put('Spouse_Remarried__c',wrapperMap.get('spouseRemarried'));
                if(wrapperMap.containsKey('dateOfRemerige'))
                    obj.put('Date_of_Remarriage__c',Date.valueof(wrapperMap.get('dateOfRemerige')));
                if(wrapperMap.containsKey('spouseDateOfRemerige'))
                    obj.put('Spouse_Date_of_Remarriage__c',Date.valueof(wrapperMap.get('spouseDateOfRemerige')));
                if(wrapperMap.containsKey('dependentRemarried'))
                    obj.put('Remarried__c',wrapperMap.get('dependentRemarried'));
                if(wrapperMap.containsKey('parentDependent'))
                    obj.put('Parent_Dependent__c',wrapperMap.get('parentDependent'));
                if(wrapperMap.containsKey('graduation'))
                    obj.put('Expected_Date_of_Graduation__c',date.valueOf(wrapperMap.get('graduation')));
                if(wrapperMap.containsKey('school'))
                    obj.put('Name_of_School__c',wrapperMap.get('school'));
                 if(wrapperMap.containsKey('dependentKey'))
                    obj.put('Dependent_Key__c',wrapperMap.get('dependentKey'));
                if(wrapperMap.containsKey('isEdited'))
                   obj.put('Added_Edited__c', Boolean.valueOf(wrapperMap.get('isEdited')));
               
                if(obj <> null) {
                  
                       upsert obj;
                 
                }

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}

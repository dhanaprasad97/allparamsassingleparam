/**
 * Created by : dhana prasad
 * Description : to demo to pass all params as single parameter to apex
 */

import { LightningElement, track, api, wire } from 'lwc';
import addDependent from "@salesforce/apex/ApexClassUtility.createDependents";

export default class SaveEntireRecordUsingSingleParamToAPex extends LightningElement {
    @track obj = {
        "firstName": 'Dhana',
        "relationShip": 'Son',
        "middleName": 'Prasad',
        "lastName": 'Macharla',
        "suffix": 'Mr.',
        "dob": '1-1-1991',
        "gender": 'M',
        "ssn": '1234589',
        "Id": '',
        "coverageId": 'a0075r826786r28Ku',
        "graduation": '',
        "school": 'High School',
        "dependentKey" : '2'
    };

    handleSave(){
        addDependent({
            dependentString: JSON.stringify(this.obj)
        })
            .then(result => {})
            .catch(error => {
                console.log('error: ', JSON.stringify(error));
            })
    }
}